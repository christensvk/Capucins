<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StageRepository")
 */
class Stage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Professeur")
     * @ORM\JoinColumn(name="idProfesseur",referencedColumnName="id")
     */

    private $professeur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tuteur")
     * @ORM\JoinColumn(name="idTuteur",referencedColumnName="id")
     */

    private $tuteur;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Eleve")
     * @ORM\JoinColumn(name="idEleve",referencedColumnName="id")
     */

    private $eleve;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getProfesseur()
    {
        return $this->professeur;
    }

    /**
     * @param mixed $professeur
     */
    public function setProfesseur($professeur): void
    {
        $this->professeur = $professeur;
    }

    /**
     * @return mixed
     */
    public function getTuteur()
    {
        return $this->tuteur;
    }

    /**
     * @param mixed $tuteur
     */
    public function setTuteur($tuteur): void
    {
        $this->tuteur = $tuteur;
    }

    /**
     * @return mixed
     */
    public function getEleve()
    {
        return $this->eleve;
    }

    /**
     * @param mixed $eleve
     */
    public function setEleve($eleve): void
    {
        $this->eleve = $eleve;
    }


}
