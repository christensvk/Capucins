<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use Symfony\Component\HttpFoundation\Request;


class EntrepriseController extends Controller
{
    /**
     * @Route("/entreprise", name="entreprise")
     */
    public function index()
    {
        $entreprise = $this->getDoctrine()
            ->getRepository(Entreprise::class)
            ->findAll();

        if (!$entreprise) {
            throw $this->createNotFoundException(
                'No entreprise found for id '
            );
        }
        return $this->render('entreprise/index.html.twig',["entreprises"=>$entreprise]);
    }

    /**
     * @Route("/entreprise/add", name="addEntreprise")
     */
    public function newEntreprise(Request $request){
        $entreprise = new Entreprise();
        $form = $this->createForm(EntrepriseType::class,$entreprise);


        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entreprise);
            $em->flush();

            return $this->redirectToRoute('stageAddEntreprise');
        }

        return $this->render('entreprise/addEntreprise.html.twig',["form"=>$form->createView()]);

    }

    /**
     * @Route("/entreprise/localisation/{id}", name="localisationEntreprise")
     */
    public function localisation(Entreprise $entreprise)
    {
        return $this->render('entreprise/localisation.html.twig',["entreprise"=>$entreprise]);
    }
}
