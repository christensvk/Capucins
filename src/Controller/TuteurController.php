<?php

namespace App\Controller;

use App\Entity\Tuteur;
use App\Form\TuteurType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class TuteurController extends Controller
{
    /**
     * @Route("/tuteur/add", name="addTuteur")
     */
    public function newTuteur(Request $request){
        $tuteur = new Tuteur();
        $form = $this->createForm(TuteurType::class,$tuteur);


        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tuteur);
            $em->flush();

            return $this->redirectToRoute('tuteur');
        }

        return $this->render('tuteur/addTuteur.html.twig',["form"=>$form->createView()]);

    }
}
