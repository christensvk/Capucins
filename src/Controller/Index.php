<?php
/**
 * Created by PhpStorm.
 * User: Siva
 * Date: 16/03/2018
 * Time: 16:20
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class Index extends Controller
{
    /**
      * @Route("/", name="index")
     */

    public function index() {
        return $this->render('base.html.twig');
    }

}