<?php

namespace App\Controller;

use App\Form\StageType;
use App\Entity\Eleve;
use App\Entity\Professeur;
use App\Entity\Stage;
use App\Entity\Entreprise;
use App\Repository\EleveRepository;
use App\Repository\TuteurRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityRepository;

use App\Entity\Tuteur;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class StageController extends Controller
{
    /**
     * @Route("/stage", name="stage")
     */
    public function index()
    {
        $stages = $this->getDoctrine()
            ->getRepository(Stage::class)
            ->findAll();

        if (!$stages) {
            throw $this->createNotFoundException(
                'No stage found'
            );
        }
        return $this->render('stage/index.html.twig',["stages"=>$stages]);
    }


    /**
     * @Route("/stage/add", name="stageAddEntreprise")
     */
    public function addEntreprise(Request $request)
    {
        $entreprise = $this->getDoctrine()
            ->getRepository(Entreprise::class)
            ->findBy([],['nom' => 'ASC']);

        if (!$entreprise) {
            throw $this->createNotFoundException(
                'No entreprise found for id '
            );
        }
        return $this->render('stage/add.html.twig',["entreprises"=>$entreprise]);
    }

    /**
     * @Route("/stage/add/{id}", name="stageAdd")
     */
    public function addStage(Request $request, $id){
        $stage = new Stage();
        $form = $this->createForm(StageType::class,$stage,
            [
                "id"=>$id
            ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $stage = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($stage);
                $em->flush();

                return $this->redirectToRoute('stage');
            }catch (UniqueConstraintViolationException $e){
                return new Response(
                    '<html><body>Vous avez déjà un stage.</body></html>'
                );
            }


        }

        return $this->render('stage/addAffectation.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
