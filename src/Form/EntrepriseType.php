<?php
/**
 * Created by PhpStorm.
 * User: Siva
 * Date: 17/03/2018
 * Time: 18:42
 */

namespace App\Form;

use App\Entity\Entreprise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class EntrepriseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('ville')
            ->add('codepostal')
            ->add('adresse')
            ->add('mail')
            ->add('tel')
            ->add('activite')
            ->add('active', HiddenType::class, array('data' => true,))
            ->add('Valider', SubmitType::class)
        ;
    }
}