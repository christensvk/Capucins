<?php
/**
 * Created by PhpStorm.
 * User: Siva
 * Date: 23/03/2018
 * Time: 16:43
 */

namespace App\Form;


use App\Entity\Eleve;
use App\Entity\Professeur;
use App\Entity\Tuteur;
use App\Repository\TuteurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tuteur', EntityType::class,
                [
                    'class' => Tuteur::class,
                    'query_builder' => function (TuteurRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                            ->andWhere('u.entreprise = :id')
                            ->setParameter('id', $options["id"]);
                    },
                    'choice_label' => 'nom'
                ]
            )
            ->add('professeur', EntityType::class,
                [
                    'class' => Professeur::class,
                    'choice_label' => 'nom'
                ]
            )
            ->add('date', DateType::class)

            ->add('eleve', EntityType::class,
                [
                    'class' => Eleve::class,
                    /*'query_builder' => function (EleveRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->andWhere('u.id = :id')
                            ->setParameter('id', 1);
                    },*/
                    'choice_label' => 'nom'
                ]
            )
            ->add('Valider', SubmitType::class)

        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'id' => null,
        ));
    }
}