<?php
/**
 * Created by PhpStorm.
 * User: Siva
 * Date: 17/03/2018
 * Time: 21:37
 */

namespace App\Form;

use App\Entity\Entreprise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TuteurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('mail',EmailType::class)
            ->add('tel')
            ->add('entreprise',EntityType::class, array('class' => Entreprise::class,'choice_label' => 'nom'))
            ->add('Valider', SubmitType::class)
        ;
    }
}