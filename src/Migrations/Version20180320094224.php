<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180320094224 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stage ADD idProfesseur INT DEFAULT NULL, ADD idTuteur INT DEFAULT NULL, ADD idEleve INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936995514C73 FOREIGN KEY (idProfesseur) REFERENCES professeur (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936935508AF2 FOREIGN KEY (idTuteur) REFERENCES tuteur (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369FB6546EA FOREIGN KEY (idEleve) REFERENCES eleve (id)');
        $this->addSql('CREATE INDEX IDX_C27C936995514C73 ON stage (idProfesseur)');
        $this->addSql('CREATE INDEX IDX_C27C936935508AF2 ON stage (idTuteur)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C27C9369FB6546EA ON stage (idEleve)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C936995514C73');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C936935508AF2');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C9369FB6546EA');
        $this->addSql('DROP INDEX IDX_C27C936995514C73 ON stage');
        $this->addSql('DROP INDEX IDX_C27C936935508AF2 ON stage');
        $this->addSql('DROP INDEX UNIQ_C27C9369FB6546EA ON stage');
        $this->addSql('ALTER TABLE stage DROP idProfesseur, DROP idTuteur, DROP idEleve');
    }
}
